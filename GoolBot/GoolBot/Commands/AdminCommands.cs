﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime;
using System.Collections;

namespace GoolBot.Commands
{
    public class AdminCommands : BaseCommandModule
    {
        [Command("dbinit")]
        public async Task WhoGool(CommandContext ctx)
        {
            await ctx.Channel.SendMessageAsync("Creating gool database").ConfigureAwait(false);

            SQLiteConnection.CreateFile("GoolDatabase.sqlite");

            var _conn = new SQLiteConnection(GoolBot.connectionString);
            await _conn.OpenAsync();

            await ctx.Channel.SendMessageAsync("Database open");

            SQLiteCommand _command = new SQLiteCommand(
                "create table gools (name varchar(30), goodmeme int, gool int)",
                _conn);

            await _command.ExecuteNonQueryAsync();
            await ctx.Channel.SendMessageAsync("Table created");

            var goolEmoji = DiscordEmoji.FromName(ctx.Client, ":gool:");

            await ctx.Channel.SendMessageAsync(goolEmoji);
        }

        [Command("addgools")]
        public async Task AddGools(CommandContext ctx)
        {
            if (ctx.User.Username == "Charlie")
            {
                var _conn = new SQLiteConnection(GoolBot.connectionString);
                await _conn.OpenAsync();

                try
                {
                    var _members = await ctx.Guild.GetAllMembersAsync();

                    foreach (var member in _members)
                    {
                        if (!member.IsBot)
                        {
                            var _command = new SQLiteCommand(
                                "insert into gools (name, gool, goodmeme) values ('" + member.Username + "', 0, 0)",
                                _conn);
                            await _command.ExecuteNonQueryAsync();
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                
                await _conn.CloseAsync();
            }
            else
                await ctx.Channel.SendMessageAsync("You are not my master, begone vile gool");
        }

        [Command("cleargools")]
        public async Task ClearGools(CommandContext ctx)
        {
            if (ctx.User.Username == "Charlie")
            {
                var _conn = new SQLiteConnection(GoolBot.connectionString);
                await _conn.OpenAsync();

                var _command = new SQLiteCommand(
                            "delete from gools",
                            _conn);
                await _command.ExecuteNonQueryAsync();

                await _conn.CloseAsync();

                await ctx.Channel.SendMessageAsync("gool database cleared");
            }
            else
                await ctx.Channel.SendMessageAsync("You are not my master, begone vile gool");
        }

        [Command("GoolScan")]
        public async Task GoolScan(CommandContext ctx)
        {
            if (ctx.User.Username == "Charlie")
            {
                await ctx.Channel.SendMessageAsync("Beginning Gool Scan, please remain where you are and don't touch anything");

                //string = name, int[0] = gool, int[1] = goodmeme
                var _tempGoolDict = new Dictionary<string, int[]>();

                var _members = await ctx.Guild.GetAllMembersAsync();

                foreach (var _member in _members)
                {
                    if (!_member.IsBot)
                        _tempGoolDict.Add(_member.Username, new int[] { 0, 0 });
                }

                int _numMessagesToGet = 1000;

                foreach (var chnl in ctx.Guild.Channels)
                {
                    if (chnl.Value.Type == DSharpPlus.ChannelType.Text)
                    {
                        var _msgs = await chnl.Value.GetMessagesAsync(_numMessagesToGet);
                        ScanMessageSet(_msgs, _tempGoolDict);
                        DiscordMessage _updateMsg = await chnl.Value.SendMessageAsync("Scanning first " + _numMessagesToGet + " messages in " + chnl.Value.Name + " for gool...");
                        int _iterations = 1;
                        do
                        {
                            try
                            {
                                if (_iterations >= 2)
                                    await _updateMsg.ModifyAsync("Scanned " + _iterations * _numMessagesToGet + " messages in " + chnl.Value.Name + " for gool...");

                                _msgs = await chnl.Value.GetMessagesBeforeAsync(_msgs.Last()?.Id ?? chnl.Value.LastMessageId, _numMessagesToGet);
                                ScanMessageSet(_msgs, _tempGoolDict);

                                _iterations++;
                                await Task.Delay(2000);
                            }
                            catch (Exception e)
                            {
                                await chnl.Value.SendMessageAsync(e.Message);
                            }
                        } while (_msgs.Count == _numMessagesToGet);
                    }
                }

                var _conn = new SQLiteConnection(GoolBot.connectionString);
                await _conn.OpenAsync();

                foreach (var _pair in _tempGoolDict)
                {
                    var _command = new SQLiteCommand(
                        "update gools " +
                        "set goodmeme = goodmeme + " + _pair.Value[1] + " " +
                        "where name ='" + _pair.Key + "'",
                        _conn);
                    await _command.ExecuteNonQueryAsync();
                    _command = new SQLiteCommand(
                        "update gools " +
                        "set gool = gool + " + _pair.Value[0] + " " +
                        "where name ='" + _pair.Key + "'",
                        _conn);
                    await _command.ExecuteNonQueryAsync();
                }

                await _conn.CloseAsync();

                await ctx.Channel.SendMessageAsync("Gool scan complete. Please move along.");
            }
            else
                await ctx.Channel.SendMessageAsync("You are not my master, begone vile gool");
        }

        private void ScanMessageSet(IReadOnlyList<DiscordMessage> messages, Dictionary<string, int[]> gools)
        {
            try
            {
                foreach (var _msg in messages)
                {
                    if (_msg.Author.Username != "Deleted User" && _msg.Author.Username != "GoolBot")
                    {
                        if (_msg.Reactions.Count > 0)
                        {
                            foreach (var _react in _msg.Reactions)
                            {
                                if (_react.Emoji.Name == "gool")
                                {
                                    gools[_msg.Author.Username][0]++;
                                }
                                else if (_react.Emoji.Name == "good_meme")
                                {
                                    gools[_msg.Author.Username][1]++;
                                }
                            }
                        }
                    }
                }
            } catch(Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
