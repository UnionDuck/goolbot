﻿using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.SQLite;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace GoolBot.Commands
{
    public class GoolCommands : BaseCommandModule
    {

        [Command("gools")]
        public async Task WhoGool(CommandContext ctx)
        {
            await WhoIsGool(ctx.Channel, ctx.Client);
        }
        
        public static async Task WhoIsGool(DiscordChannel chnl, DiscordClient client)
        {
            var _conn = new SQLiteConnection(GoolBot.connectionString);
            await _conn.OpenAsync();

            var _command = new SQLiteCommand(
                "select * from gools",
                _conn);

            SQLiteDataReader _reader = _command.ExecuteReader();

            List<MarkdownTableItem> _tableData = new List<MarkdownTableItem>();         

            while (_reader.Read())
            {
                _tableData.Add(new MarkdownTableItem((int)_reader["gool"], (int)_reader["goodmeme"], _reader["name"].ToString()));
            }

            await _conn.CloseAsync();

            await chnl.SendMessageAsync("```\n" + _tableData.ToMarkdownTable() + "\n```");

            var goolEmoji = DiscordEmoji.FromName(client, ":gool:");

            await chnl.SendMessageAsync(goolEmoji);
        }

        public static async Task GoolStats(DiscordChannel chnl, DiscordClient client)
        {
            var _conn = new SQLiteConnection(GoolBot.connectionString);
            await _conn.OpenAsync();

            var _command = new SQLiteCommand(
                "select * from gools",
                _conn);

            SQLiteDataReader _reader = _command.ExecuteReader();

            List<MarkdownTableItem> _tableData = new List<MarkdownTableItem>();

            while (_reader.Read())
            {
                _tableData.Add(new MarkdownTableItem()
                {
                    Name = _reader["name"].ToString(),
                    Good_Meme = (int)_reader["goodmeme"],
                    Gool = (int)_reader["gool"],
                    Ratio = 0
                });
            }
           
            await _conn.CloseAsync();
        }


    }
}
