﻿namespace GoolBot
{
    public struct MarkdownTableItem
    {        
        public int Gool;
        public int Good_Meme;
        public int Ratio;
        public string Name;

        public MarkdownTableItem(int Gool, int Good_Meme, string Name)
        {
            this.Name = Name;
            this.Gool = Gool;
            this.Good_Meme = Good_Meme;

            if (Good_Meme != 0)
            {
                if(Gool != 0)
                {
                    Ratio = (int)((float)(Good_Meme / (float)(Good_Meme + Gool)) * 100f);
                }
                else
                {
                    Ratio = 100;
                }
            }               
            else
                Ratio = 0;
        }
    }
}