﻿using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.CommandsNext;
using DSharpPlus.EventArgs;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Extensions;
using GoolBot.Commands;
using Newtonsoft.Json;
using System;
using System.Data.SQLite;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace GoolBot
{
    public class GoolBot
    {
        public static string connectionString = "Data Source=GoolDatabase.sqlite;Version=3;";

        public DiscordClient Client { get; private set; }
        public CommandsNextExtension Commands { get; private set; }

        public InteractivityConfiguration Interactivity { get; private set; }

        private int LastMonthWished = 0;

        public async Task RunAsync()
        {
            var json = string.Empty;

            using (var fs = File.OpenRead("config.json"))
            {
                using var sr = new StreamReader(fs, new UTF8Encoding(false));
                    json = sr.ReadToEnd();
            }

            var configJson = JsonConvert.DeserializeObject<ConfigJson>(json);

            var config = new DiscordConfiguration
            {
                Token = configJson.Token,
                TokenType = TokenType.Bot,
                AutoReconnect = true,
                MinimumLogLevel = Microsoft.Extensions.Logging.LogLevel.Debug
            };

            Client = new DiscordClient(config);

            Client.Ready += Client_Ready;

            Client.UseInteractivity(new InteractivityConfiguration
            {

            });

            var commandsConfig = new CommandsNextConfiguration
            {
                StringPrefixes = new string[] { configJson.Prefix },
                EnableMentionPrefix = true,
                EnableDms = false,
                IgnoreExtraArguments = true
            };

            Commands = Client.UseCommandsNext(commandsConfig);

            Commands.RegisterCommands<GoolCommands>();
            Commands.RegisterCommands<AdminCommands>();

            Client.MessageReactionAdded += Client_MessageReactionAdded;

            Client.MessageCreated += Client_MessageCreated;            

            await Client.ConnectAsync();
            await Task.Delay(-1);
        }

        private async Task Client_MessageCreated(DiscordClient sender, MessageCreateEventArgs e)
        {
            var msgContent = e.Message.Content;
            
            if(msgContent.ToLower().Contains("happy new month fan") && e.Author.Username != "GoolBot")
            {
                _ = await new DiscordMessageBuilder()
                            .WithContent("Happy New Month Fan")
                            .HasTTS(true)
                            .SendAsync(e.Channel);

                using (var filestream = new FileStream("hnmf.jpg", FileMode.Open, FileAccess.Read))
                {
                    _ = await new DiscordMessageBuilder()
                        .WithFiles(new System.Collections.Generic.Dictionary<string, Stream>() { { "hnmf.jpg", filestream } })
                        .SendAsync(e.Channel);
                }
            }
            
            if (msgContent.StartsWith("GoolBot") || msgContent.StartsWith("goolbot") && e.Message.Content.Contains("purpose"))
            {
                await e.Channel.SendMessageAsync("I am GoolBot. I count Gool.");
            }
            else if (msgContent.StartsWith("GoolBot") || msgContent.StartsWith("goolbot") && e.Message.Content.Contains("who") && e.Message.Content.Contains("gool"))
            {
                await GoolCommands.WhoIsGool(e.Channel, sender);
            }
        }

        private async Task Client_MessageReactionAdded(DiscordClient sender, MessageReactionAddEventArgs e)
        {
            if (e.Emoji.Name == "gool")
            {
                var _msg = await e.Channel.GetMessageAsync(e.Message.Id);

                var _conn = new SQLiteConnection(connectionString);
                await _conn.OpenAsync();

                var _command = new SQLiteCommand(
                    "update gools " +
                    "set gool = gool + 1 " +
                    "where name ='" + _msg.Author.Username + "'",
                    _conn);

                //e.Message.

                await _command.ExecuteNonQueryAsync();

                await _conn.CloseAsync();
            }
            else if (e.Emoji.Name == "good_meme")
            {
                var _msg = await e.Channel.GetMessageAsync(e.Message.Id);
                var _conn = new SQLiteConnection(connectionString);
                await _conn.OpenAsync();

                var _command = new SQLiteCommand(
                    "update gools " +
                    "set goodmeme = goodmeme + 1 " +
                    "where name ='" + _msg.Author.Username +"'",
                    _conn);

                await _command.ExecuteNonQueryAsync();

                await _conn.CloseAsync();
            }
        }

        private async Task Client_Ready(DiscordClient sender, ReadyEventArgs e)
        {
            if (DateTime.Today.Day == 1)
            {
                if (DateTime.Today.Month != LastMonthWished)
                {
                    LastMonthWished = DateTime.Today.Month;

                    foreach (var _server in sender.Guilds.Values)
                    {
                        var _member = await _server.GetMemberAsync(219915558529794050);
                        if(_member != null)
                        {
                            var _msg = new DiscordMessageBuilder()
                                .WithContent("Happy New Month Fan")
                                .HasTTS(true);
                            await _member.SendMessageAsync(_msg);

                            using (var fs = new FileStream("hnmf.jpg", FileMode.Open, FileAccess.Read))
                            {
                                _msg = new DiscordMessageBuilder()
                                    .WithFiles(new System.Collections.Generic.Dictionary<string, Stream>() { { "hnmf.jpg", fs } });
                                await _member.SendMessageAsync(_msg);
                            }
                                
                            return;
                        }
                        
                    }
                }
            }
        }
    }
}
