﻿using System;

namespace GoolBot
{
    class Program
    {
        static void Main(string[] args)
        {
            var bot = new GoolBot();
            bot.RunAsync().GetAwaiter().GetResult();
        }
    }
}
